package ass.java.single;

public class SinglePattern {

	private static SinglePattern myObj;

	private SinglePattern() {

	}

	public static SinglePattern getInstance() {
		if (myObj == null) {
			myObj = new SinglePattern();
		}
		return myObj;
	}

	public void getSomeThing() {

		System.out.println("I am here....");
	}

	public static void main(String a[]) {
		SinglePattern st = SinglePattern.getInstance();
		st.getSomeThing();
	}
}
