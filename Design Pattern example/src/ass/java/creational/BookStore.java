package ass.java.creational;

public class BookStore {
	public Store getStore(String store) {
        if(store.equals(null)) return null;

        if(store.equalsIgnoreCase("Book")) {
            return new Book();
        } else if(store.equalsIgnoreCase("DVD")) {
            return new DVD();
        } else if(store.equalsIgnoreCase("Softwares")) {
            return new Softwares();
        }
        return null;        
    }  
}
