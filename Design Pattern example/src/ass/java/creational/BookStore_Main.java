package ass.java.creational;

public class BookStore_Main {
	public static void main(String[] args) {
		BookStore bookstore = new BookStore();
		Store store = bookstore.getStore("Book");
		store.Items();
		Store store2 = bookstore.getStore("DVD");
		store2.Items();
		Store store3 = bookstore.getStore("Softwares");
		store3.Items();

	}

}
