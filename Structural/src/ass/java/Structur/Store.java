package ass.java.Structur;

public abstract class Store {
	protected BuyDetails buydetails;

	protected Store(BuyDetails buydetails) {
		this.buydetails = buydetails;
	}

	public abstract void collect();

}
