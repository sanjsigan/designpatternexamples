package ass.java.Structur;

public class BookStore extends Store {
	private int cash, Number;
	private String Name;

	public BookStore(int cash, int Number, String Name, BuyDetails buydetails) {
		super(buydetails);
		this.cash = cash;
		this.Number = Number;
		this.Name = Name;

	}

	public void collect() {
		buydetails.Cash(cash, Number, Name);
	}

}
