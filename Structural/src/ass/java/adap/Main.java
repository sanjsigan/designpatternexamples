package ass.java.adap;

public class Main {
	public static void main(String args[]) {
		Parrot parrot = new Parrot();
		ToyDuck toyDuck = new PlasticToyDuck();

		ToyDuck birdAdapter = new BirdAdapter(parrot);

		System.out.println("Sparrow...");
		parrot.fly();
		parrot.makeSound();

		System.out.println("ToyDuck...");
		toyDuck.squeak();

		System.out.println("BirdAdapter...");
		birdAdapter.squeak();
	}

}
